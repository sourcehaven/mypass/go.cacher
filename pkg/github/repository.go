package github

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
)

func Create(accessToken, repoName string) error {

	// Set the GitHub API endpoint and the request payload
	apiURL := "https://api.github.com/user/repos"
	payload := []byte(fmt.Sprintf(`{"name":"%s"}`, repoName))

	// Create an HTTP request
	req, err := http.NewRequest("POST", apiURL, bytes.NewBuffer(payload))
	if err != nil {
		return err
	}

	// Set the Authorization header with the access token
	req.Header.Set("Authorization", "token "+accessToken)
	req.Header.Set("Content-Type", "application/json")

	// Make the HTTP request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
		}
	}(resp.Body)

	return nil
}
