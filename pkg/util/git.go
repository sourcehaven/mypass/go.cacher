package util

import (
	"strings"
)

func GetRepoNameFromUrl(url string) string {
	// Extract the path from the URL
	pathParts := strings.Split(url, "/")

	// The repository name is the last part of the path
	repoName := pathParts[len(pathParts)-1]

	// Remove any ".git" suffix if present
	repoName = strings.TrimSuffix(repoName, ".git")

	return repoName
}
