package cache

type Cacher interface {
	Persist(bytes []byte) (err error)
	Get() (data []byte, err error)
}
