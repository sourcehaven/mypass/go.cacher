package cache

import (
	"github.com/go-git/go-billy/v5/memfs"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/config"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/go-git/go-git/v5/storage/memory"
	"gitlab.com/sourcehaven/mypass/go.cacher/pkg/github"
	"gitlab.com/sourcehaven/mypass/go.cacher/pkg/util"
	"io"
	"strings"
)

type GitCacheConfig struct {
	RemoteUrl     string
	DefaultBranch string
	Credentials   Credentials
}

type gitCache struct {
	Config *GitCacheConfig
	Repo   *git.Repository
	Remote *git.Remote
}

func NewGitCache(c ...*GitCacheConfig) (Cacher, error) {
	var repo *git.Repository
	var err error
	var cfg *GitCacheConfig

	if len(c) > 0 {
		cfg = c[0]
	}

	repo, err = clone(cfg.RemoteUrl, cfg.Credentials)
	if err != nil {
		if err.Error() == "repository not found" {
			repo, err = createRepo()
			if err != nil {
				return nil, err
			}

			err = createRemote(repo, RemoteName, cfg.RemoteUrl)
			if err != nil {
				return nil, err
			}
		} else {
			return nil, err
		}
	}

	remote, err := repo.Remote(RemoteName)
	if err != nil {
		return nil, err
	}

	return &gitCache{cfg, repo, remote}, nil
}

func createRepo() (*git.Repository, error) {
	fs := memfs.New()
	store := memory.NewStorage()

	return git.Init(store, fs)
}

func createRemote(repo *git.Repository, remoteName, remoteUrl string) error {
	_, err := repo.CreateRemote(&config.RemoteConfig{
		Name: remoteName,
		URLs: []string{remoteUrl},
	})
	return err
}

func clone(remoteUrl string, cred Credentials) (*git.Repository, error) {
	store := memory.NewStorage()
	fs := memfs.New()

	return git.Clone(
		store,
		fs,
		&git.CloneOptions{
			URL:   remoteUrl,
			Auth:  cred.GetAuth(),
			Depth: 1,
		},
	)
}

func (c *gitCache) Persist(bytes []byte) (err error) {
	tree, err := c.Repo.Worktree()

	if err != nil {
		return err
	}

	file, err := tree.Filesystem.Create(FileName)
	if err != nil {
		return err
	}

	_, err = file.Write(bytes)
	if err != nil {
		return err
	}

	_, err = tree.Add(file.Name())
	if err != nil {
		return err
	}

	now := getNow()
	_, err = tree.Commit("Auto-commit: Updating "+FileName,
		&git.CommitOptions{
			Author: &object.Signature{
				Name:  "git-bot",
				Email: "git@bot.org",
				When:  now,
			},
			AllowEmptyCommits: false,
		})
	if err != nil {
		return err
	}

	err = c.push()

	if err != nil {
		if err.Error() == "repository not found" && strings.Contains(c.Config.RemoteUrl, "github.com") {
			repoName := util.GetRepoNameFromUrl(c.Config.RemoteUrl)

			err := github.Create(c.Config.Credentials.Password, repoName)
			if err != nil {
				return err
			}

			err = c.push()
			if err != nil {
				return err
			}
		} else {
			return err
		}
	}
	return nil

}

func (c *gitCache) push() error {
	err := c.Remote.Push(&git.PushOptions{
		RemoteName: RemoteName,
		RemoteURL:  c.Config.RemoteUrl,
		Auth:       c.Config.Credentials.GetAuth(),
	})
	return err
}

func (c *gitCache) Get() (data []byte, err error) {

	tree, err := c.Repo.Worktree()
	if err != nil {
		return nil, err
	}

	file, err := tree.Filesystem.Open(FileName)
	if err != nil {
		return nil, err
	}
	defer func(file io.Closer) {
		err := file.Close()
		if err != nil {
		}
	}(file)

	data, err = io.ReadAll(file)
	if err != nil {
		return nil, err
	}

	return data, nil

}
