package cache

import "github.com/go-git/go-git/v5/plumbing/transport/http"

type Credentials struct {
	Username string
	Password string
}

func (c *Credentials) GetAuth() *http.BasicAuth {
	return &http.BasicAuth{
		Username: c.Username,
		Password: c.Password,
	}
}
