package cache

import "time"

func getNow() time.Time {
	return time.Now()
}
